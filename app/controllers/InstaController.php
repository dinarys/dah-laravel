<?php

class InstaController extends BaseController {

	protected $insta;

	public function __construct(Insta $insta){

		$this->insta = $insta;
	}

	public function index(){
		$insta = $this->insta->orderBy('id','ASC')->lists('thumb');
		$insta = json_encode($insta);
		return View::make('admin_interface.insta.index',compact('insta'));
	}

	public function store(){
		$validation = Validator::make(Input::all(),Insta::$rules);
		if ($validation->passes()):
			if(self::save()){
				return json_encode("Данные успешно сохранены.", JSON_UNESCAPED_UNICODE);
			}else {
				return json_encode("Не сохранено.", JSON_UNESCAPED_UNICODE);
			}
		endif;
		return json_encode("Ошибка: $validation", JSON_UNESCAPED_UNICODE);
	}

	private function save($insta = NULL){
		$this->insta->truncate();
		if(is_null($insta)):
			$insta = $this->insta;
		endif;
		array_map(function($data){
			foreach ($data as &$row) {
				$now_time = Carbon\Carbon::now();
				$row['created_at'] = $now_time->toDateTimeString();
			}
			if(!DB::table('insta')->insert($data))
				return false;
		}, Input::all());
		return true;
	}

}
